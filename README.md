# qouesm's TF2 config

**This config assumes you have [mastercomfig](https://mastercomfig.com/) installed.**

## Requirements

mastercomfig alters default locations for certain config files [[1]](https://docs.mastercomfig.com/latest/setup/install/#custom-configs) [[2]](https://docs.mastercomfig.com/latest/faq/#what-is-the-mastercomfig-way-of-doing-things).

## Intro

The majority of my config is based on woolen's config  and other scripts [[3]](https://dropbox.com/s/y5ki4urvvz9igja/WoolenConfigs.zip%3Fdl%3D1&v=cRGW4a1K_Io) [[4]](https://www.youtube.com/watch?v=cRGW4a1K_Io).
The core philosophy is to reduce input complexity or context sentitivity [[5]](https://en.wikipedia.org/wiki/Context-sensitive_user_interface) [[6]](https://www.youtube.com/watch?v=pl9G4voWfDQ).
For my TF2 config, this means that one button is assigned to one weapon.
There are exceptions to these *rules* because all of this config shows off my fine-tuned preferences, but the core idea is that left click (m1) will pull out, if not already out, and shoot my primary weapon, right click (m2) will do the same for my secondary, and melee is bound to mouse4 or mouse5 (m4) (m5).

## Footnotes

- On my mouse, pressing m4 will send code m3. **m4->m3; m5->m4; m3->m5**. Essentially, they all cycle so that m3 can be accessed with my thumb for more efficient web browsing.
If you want to adapt these configs to default mouse bindings, change: `mouse3 -> mouse4; mouse4 -> mouse5; mouse5 -> mouse3`.

- I use an ergonomic keyboard with 48 keys, which includes no number row and 6 thumb keys.
While this does affect the number of keys I can access quickly and affect my config, I previously used a version of this config on larger keyboards with very few changes from this.
Because my config does not rely on number keys outside of two I access on a layer when playing Engineer, it also is a very ideal setup for keyboards without numrows.

## Explaination

### game_overrides

**Note**: mastercomfig allows users to define a class config file that runs by default for all classes in a file under `tf/cfg/overrides/game_overrides.cfg` [[5]](https://docs.mastercomfig.com/latest/customization/custom_configs/).

Much of this is default or otherwise includes minor rebinds so I'll only cover unique bindings

```
bind "/" "incrementvar tf_hud_target_id_disable_floating_health  0 1 1"
```
Toggles player floating health, primarily used to see bot health in MvM

```
bind "F1" "load_itempreset 0;"
bind "F2" "load_itempreset 1;"
bind "F3" "load_itempreset 2;"
bind "F4" "load_itempreset 3;
```
F1-F4 are used to switch between the four "ABCD" loadouts each class is given.
While useful for changing from defensive Engineer to battle Engineer for instance, this most frequently is used to quickly "resupply", as this bind will let you respawn with full health and ammo while in spawn without touching the resupply cabinet.
This also lets you respawn to a new forward spawn if your team captures a point.

```
bind KP_END "join_class scout"
bind KP_DOWNARROW "join_class soldier"
bind KP_PGDN "join_class pyro"
bind KP_LEFTARROW "join_class demoman"
bind KP_5 "join_class heavyweapons"
bind KP_RIGHTARROW "join_class engineer"
bind KP_HOME "join_class medic"
bind KP_UPARROW "join_class sniper"
bind KP_PGUP "join_class spy"
```
This binds your numpad to switch to the nine classes.
This remains unused as I don't have a numpad anymore.

```
bind "p" "incrementvar r_drawviewmodel 0 1 1"
```
Toggles viewmodel visibility.

```
bind "w" "+mf"
bind "a" "+ml"
bind "s" "+mb"
bind "d" "+mr"
```
Required to implement mastercomfig's null-cancelling movement addon, whereby pressing two opposing directions does not stop movement.

```
exec scoreboard;
```

Brings up various info like fps when looking at the scoreboard.

```
bind "[" hud_reloadscheme
bind "]" "helpme"
alias "helpme" "record demo ; stop ; snd_restart ; hud_reloadscheme"
```

`[` will reload the HUD to fix HUD issues.
`]` will do the same but also restart sound as well as start and stop a demo to fix things like invisible players and stuck sounds.

### Scout

- m1: pull and shoot primary
- m2: pull and shoot secondary
- m3: pull and use melee
- m4: altfire; uses sandman ball or soda popper charge 
- m5: fast milk bind. Unused as m2 is functionality identical

### Soldier

- m1: pull and shoot primary
- m2: pull and shoot secondary
- m3: pull and use melee
- m4: altfire; uses cow mangler charge

### Pyro

- m1: pull and shoot primary
- m2: pull primary and airblast/use phlog charge
- m3: pull and use secondary
- m4: pull and use melee
- m5: "panic": pull and shoot primary, also extremely increases mouse sensitivity and spins right to spam flames. More funny than useful
- e: pull out and altfire secondary. This is used for the detonator

### Demoman

- m1: pull and shoot primary
- m2: pull and shoot secondary
- m3: altfire; detonate stickies/shield charge
- m4: pull and use melee

### Heavy

- m1: pull and shoot primary
- m2: pull and use secondary
    - If minigun rev down animation is not completely finished, pressing this may rev the minigun instead of pulling out secondary. Consider using mouse wheel up
- m3: altfire; revs minigun
- m4: pull and use melee
- m5: pull out and throw sandvich. Can be held for both actions

### Engineer

- m1: pull and shoot primary
- m2: pull and use melee
- m3: pull and use secondary
- m4: pull and use melee
- m5: primary attack. Used to place buildings and wrangler primary fire
- quick-build macros: will destroy a and attempt to build a specific building
    - q: sentry
    - e: dispenser
    - 2: teleporter entrance
    - 4: teleporter exit
- r: eureka effect teleport to spawn
- shift+r: eureka effect teleport to exit

### Medic

- m1: pull and use medigun
- m2: pull and use primary
- m3: pull and use uber
  - Will also use a quiet voiceline (c->3) so as to conceal an uber from the enemy team.
- m4: pull and use melee
- m5: use projectile shield in MvM
- other: auto callout
    - pulling out the primary will also temporarily adjust the auto teammate health callout such that you are able to see nearby teammates, including through walls.

### Sniper

- m1: pull and shoot primary
- m2: pull and shoot secondary
- m3: altfire; rifle zoom
- m4: pull and use melee
- m5: toggles post-fire rezoom

### Spy

- m1: pull and shoot primary
- m2: pull and use melee
- m3: altfire; cloak
- m4: pull and use sapper. Can be held for both actions
- m5: disguise as enemy scout
    - Note: other than noted below, I do not have any other disguises bound. While it is possible to fool other players with disguises, I do not play Spy enough and I'm not skilled enough for this to be matter. Scout is objectively the best disguise for minimizing visibility, avoiding headshots, and fooling sentries [[6]](https://www.youtube.com/watch?v=J8x_PKItTbc).
- q: undisguise without attacking; disguises as friendly spy. 
- mouse wheel up: checks the enemy's medigun
    - disguises as enemy medic and pulls out sapper. Bound to scroll wheel as this macro needs to run a few times to function.
- mouse wheel down: disguise as friendly scout

### Everything Else

- `autoexec`
    - My autoexec is commented and mostly not worth discussing. Things like graphics and network settings are handled by mastercomfig. Also defined are my launch options, mostly taken from mastercomfig's recommendations
- `antivirus`
    - Disables community servers from downloading content, playing loud sounds, and otherwise being annoying.
- `jump`
    - Gives infinite ammo/health, and binds `q` to noclip for jump maps

- `talk/notalk`
    - Enables/disables voice and text chat. Personally unused

- `nullmove`
    - Unused since mastercomfig has since implemented null-cancelling movement but otherwise functional and defines a crouch-jump macro if you don't explosive jump frequently

- `scoreboard`
    - Called from `game_overrides.cfg`. Will show net_graph, position, and fps when looking at the scoreboard

## Extended thoughts and ramblings

TF2 is an old game with bad default settings and other configuration [citation: ask anyone with over 1000 hours].
It is my opinion that mastercomfig should be default behavior given the wholistic rework that it provides for load times, stutters, FPS, and other network settings in addition to the graphics presets it provides.
If you think mastercomfig is just another max FPS config like Comanglia's, I would strongly consider you read the extensive documentation mastercoms provides for this project [[7]](https://docs.mastercomfig.com/latest/).

As far as the effects of overhauling my class configs, It's night and day a much better experience to play with.
I'm not a competitive player but the relative ease with which this allows me to do what I want to do without the controls getting in the way is at worst a nice to have and at best is more ergonomic and allows me to play more to my potential.
When playing other shooters that use number keys and/or the scroll wheel to change between weapons, I can really notice the controls getting in the way.
Even if I know all the weapons and in which order they are equipped, using the numrow forces my hand off of WASD which often is not be an option in intense firefights at risk of losing, and using the scroll wheel can be imprecise, and that's not to mention how games in my experience are 50/50 for the default scroll direction being either next or previous weapon.
Outside of woolen whom I based my config off of, I have never known anyone else to play in this way.
I've seen some classic Quake players who will bind spacebar or m1 to movement keys which is a topic for someone else to discuss.
It's for this reason that I'm taking the time to explain why this unique style of playing is so interesting.
Also my friends constantly are confused at however the hell *this* works in addition to the more standard Engineer quickbuild and Medic auto-callout stuff works (hi if you're reading this).

Low context sensitivity is beneficial for all classes.
Even for a class like Soldier who has the simplest config, being able to reliably - more reliably than missing a scroll input - get a specific weapon out in a quick situation has basically eliminated that feeling like a death or a missed kill or a failed capture wasn't my fault.
And like I said before, the numrow just isn't an option if I'm trying to pull out my Market Gardener for a kill while trying to line up my air strafe.
As a tier V rocket jumper, being able to more reliably hit shotgun buttons to open doors mid-jump while being able to keep an air-strafe is indispensable.
I could say the same for Pyro given that is extremely safe for me to have my flare gun out most of the time given that a rocket reflect is one button away.
More complex classes like Engineer took more time to develop especially given my unique keyboard layout.
`q` and `e` mispresses, happen every so often, but it's exceedingly rare given that I'm essentially never going to hit those keys on accident.
Misfires usually are a result of zoning out and pressing one instead of the other anyway.
I've bound quickbuild sentry and dispenser to those keys and haven't had any issues.
`2` and `4` are my two teleporter quickbuild keys which I access on a layer.
I access the layer with my pinky around where shift is on most keyboard and the two macros are bound to where `a` and `d` are, which results in less finger movement.
Additionally, I have a Eureka Effect macro that sends me either to spawn with just `r` or my teleporter exit with `shift+r`.
Combine all of that with the fact that I don't have the disguise kit bound as Spy and I never have to look at a pop-up menu which could distract me or block my vision.

Does this all sound like a lot?
Of course it is!
I bound a button on Pyro to spin like a Beyblade so that *sometimes* it's easier to catch a Spy.
I have a single button I can press to throw someone a Sandvich no matter what I'm doing.
I mean I even just told you that I bound quickbuilding my teleporters to, on a layer anyway, are my two strafe keys just so that I can save myself moving half an inch off my movement keys to do something that almost never requires quick snappy execution.
Yes it *is* crazy, good observation.
But I also have almost 4000 hours in this game so I'm somewhat past that point already.
This is to say though, if you have thousands of hours in this game like many thousands do, I think you owe it to yourself to look into custom configs, and I don't just mean mastercomfig (though you should already have that installed) or even that you go insofar as to completely redefine your input philosophy like I have.
Defining an autoexec to ensure mouse acceleration is turned off for instance isn't going to hurt but it sure could help.
If you otherwise read through all of this, why?

Other random thoughts I couldn't fit in:

Attention Windows users, if you're thinking about customizing your configs, and even if you aren't: TF2 and sometimes community servers have a habit of modifying your `config.cfg`, which is the file where your options are saved to.
If you've ever felt like you kept turning some option on and it not sticking, that's why.
Consider editing that file manually, setting all the options you're after, and the important part, setting it to read-only under properties.
Having an autoexec is also useful for similar reasons.

I have sprays off for one main reason.
While sprays are 99% of the time not funny, distracting, serve to inflame, or are illegal in some way, for the upside that you get a chuckle every so often, having sprays enabled means that everytime a player joins, I am forced to download it.
You could make the argument that automatically downloading potentially illegal images to your computer is not a great idea, I don't even care about that.
What I do care about is that my game would stutter frequently when players joined, which is dumb and I hate it.

Currently I'm using SunsetHUD [[8]](https://github.com/Hypnootize/sunsethud). Especially in years past I would often change HUDs every few months or weeks as I liked the change I enjoyed seeing how different people made decisions on how to best show the user what was important.
If it hadn't become outdated, I would almost certainly be using mkhud [[9]](https://github.com/kmarcie/mkhud). It's been out of date for at least 3-4 years and so I can't recall specifics but it did almost everything perfect in my opinion.
It was smart about layouts, where it put information, the backpack loadout screens were excellent, it had great design and a few misaligned elements that I looked past because the project was a joy to use.
I have neither the time nor interest to update it myself especially when Hypnootize's huds come with modern creature comforts like a speedometer but maybe someday.

Now for my thoughts on Dustbowl...
